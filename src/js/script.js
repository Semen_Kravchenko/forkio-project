const iconWrapper = document.querySelector('.burger');
const icon = document.querySelector('.burger__icon');
const menu = document.querySelector('.menu');

iconWrapper.addEventListener('click', function() {
    icon.classList.toggle('burger__icon--active');
    menu.classList.toggle('menu--active');
});
